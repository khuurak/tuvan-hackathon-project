<?php

// Вывод на экран массива
function debug($array_) {
    if( !empty($array_) ) {
        echo '<pre>';
        print_r($array_);
        echo '</pre>';
    }

    return false;	
}

function redirect($http = false){
    if($http) {
        $redirect = $http;
    } else {
        $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : PATH;
    }

    header("Location: $redirect");
    exit;
}

// Экранирование специальных символов в строке для использования в SQL-выражении (htmlspecialchars)
// 
function verify_text(string $str){
    $str = clear_tag_script($str);
    return htmlspecialchars($str, ENT_QUOTES);
}

// Убрать из текста тег script с кодом внутри
function clear_tag_script(string $var){
	$var = preg_replace("/\<script.*?\<\/script\>/", "", $var);
	return $var;
}

// Занесение данных в файл спамов
function write_spam(string $text = NULL, string $url = NULL) {
    return error_log("[" . date('d.m.Y H:i:s') . "] Текст ошибки: {$text} | Ссылка: {$url}. \n===\n", 3, ROOT.'/tmp/spam.log');
}

// Перебор массива данных состоящих из строк и ключей с другой проверяющей строкой для получения значения массива
function enumaration_values(array $refer_array, string $sorting, string $relations_string){
    foreach($refer_array as $v) {
        if( $v->sorting == $sorting && $v->key_name == $relations_string ) {
            return $v->title;
        }
    }
}

// Перебор массива данных региона по ID для получения названия региона
function enumaration_region(array $refer_array, $region_id){
    $region_id = (int) $region_id;
    foreach($refer_array as $v) {
        if( $v["code"] == $region_id ) {
            return $v["title"];
        }
    }
}

// Перебор массива данных состоящих из строк и ключей с другой проверяющей строкой для получения значения массива
function enumaration_badges(array $refer_array, string $sorting = NULL,  string $str){
    $arr2 = explode(",", $str);
    foreach($refer_array as $v) {
        foreach($arr2 as $k2 => $v2) {
			if( $v->sorting == $sorting && $v->key_name == $v2 ) {
                echo '<span class="badge badge-secondary p-1 mr-1">' . $v->title . '</span>';
            }
        }
    }
}

// Цикл для выпадающих списков
function foreach_list_options(array $refer_array, string $sorting) {
    // Пустая переменная
    $rtrn = '';
    
    foreach($refer_array as $v) {
        if( $v->sorting == $sorting) { $rtrn .= "<option value='" . $v->key_name . "'>" . $v->title . "</option>"; }
    }

    return $rtrn;
}

// Цикл для выпадающих списков с выбором
function foreach_list_options_selected(array $refer_array, string $sorting, string $selected) {
    // Пустая переменная
    $rtrn = '';
    
    foreach($refer_array as $v) {
        if( $v->sorting == $sorting) { 
            if($v->key_name == $selected  ) {
                $rtrn .= "<option value='" . $v->key_name . "' selected>" . $v->title . "</option>";
            } else {
                $rtrn .= "<option value='" . $v->key_name . "'>" . $v->title . "</option>";
            }
        }
    }

    return $rtrn;
}

// Перевод значений дат и времени на русский язык
function rus_date() {
    // Перевод
    $translate = array(
        "am" => "дп",
        "pm" => "пп",
        "AM" => "ДП",
        "PM" => "ПП",
        "Monday" => "Понедельник",
        "Mon" => "Пн",
        "Tuesday" => "Вторник",
        "Tue" => "Вт",
        "Wednesday" => "Среда",
        "Wed" => "Ср",
        "Thursday" => "Четверг",
        "Thu" => "Чт",
        "Friday" => "Пятница",
        "Fri" => "Пт",
        "Saturday" => "Суббота",
        "Sat" => "Сб",
        "Sunday" => "Воскресенье",
        "Sun" => "Вс",
        "January" => "январь",
        "Jan" => "янв",
        "February" => "февраль",
        "Feb" => "фев",
        "March" => "март",
        "Mar" => "мар",
        "April" => "апрель",
        "Apr" => "Апр",
        "May" => "май",
        "May" => "май",
        "June" => "июнь",
        "Jun" => "июн",
        "July" => "июль",
        "Jul" => "июл",
        "August" => "август",
        "Aug" => "авг",
        "September" => "сентябрь",
        "Sep" => "сен",
        "October" => "октябрь",
        "Oct" => "окт",
        "November" => "ноябрь",
        "Nov" => "ноя",
        "December" => "декабрь",
        "Dec" => "дек",
        "st" => "ое",
        "nd" => "ое",
        "rd" => "е",
        "th" => "ое"
    );

    // Если передали дату, то переводим ее
    // Иначе передаем текущую дату
    if (func_num_args() > 1) {
        $timestamp = func_get_arg(1);
        return strtr(date(func_get_arg(0), $timestamp), $translate);
    } else {
        return strtr(date(func_get_arg(0)), $translate);
    }
}

// Первую букву в заглавную, а все остальные прописные
function ucfirst_strtolower(string $str) {
    return mb_substr(mb_strtoupper($str,'utf-8'),0,1,'utf-8').mb_strtolower(mb_substr($str,1,mb_strlen($str,'utf-8'),'utf-8'),'utf-8');
}

// Обрезаем длинную строку и добавляем в конец точки
function cut_long_str(string $str, int $count_max = 35 ) {
    // Если строка больше указанной длины + 3 (3 точки в конце), то обрезаем 
    // В протичвном случае отправляем строку без обрезки
    if ( mb_strlen($str, 'utf-8') > ($count_max + 3) ){
        // Обрезаем строку до длины из параметра - mb_substr()
        // Удаляем пробелы в начале и конце строки - trim()
        // Конкатенацией дописываем в конец три точки
        return trim(mb_substr($str, 0, $count_max, 'utf-8')) . "...";
    }
    
    // Отправка строки без обрезки
    return $str;
}

// Функция отображающая дату и время активности пользователя как в соц. сетях (минут/часов/дней назад)
// Применяетя в функции onlinestatus_date()
function onlinestatus_date_time_elapsed($datetime, $full = false) {
    // Установка дат
    $now = new DateTime;
    $ago = new DateTime($datetime);
    // Разница между текущей датой и датой параметра $datetime
    $diff = $now->diff($ago);

    // Округляет дробь в меньшую сторону
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    
    // Массив со значениями
    $string = array(
                    'y' => 'г.',
                    'm' => 'мес.',
                    'w' => 'нед.',
                    'd' => 'дн.',
                    'h' => 'ч.',
                    'i' => 'мин.',
                    's' => 'сек.');
    foreach ($string as $k => &$v) {
        if ($diff->$k) 
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        else
            unset($string[$k]);
    }
    // Если True, то показывает промежуток полностью до секунды
    if (!$full) $string = array_slice($string, 0, 2);
    return $string ? implode(' ', $string) . ' назад' : 'сейчас';
}

// Функция для получения онлайн статуса пользователя по дате из БД. 
// Если активность пользователя больше чем одна 1 минута от вычтенного времени и меньше чем текущее время, а статус равен 1, то значит В сети
// Если не было активности больше чем 1 минуту, то пользователь Не в сети
function onlinestatus_date($date, $timezone_user, $onlinestatus) {
    
    // Конвертация Даты и время активности пользователя из MySQL в UNIX с помощью PHP strtotime()
    $date_activitystatus_UNIX = strtotime($date); 

    // Установка временной зоны пользователя для PHP функции даты
    date_default_timezone_set($timezone_user);

    // Текущая дата и время по PHP
    $date_now_UNIX = time();

    // Дата и время на минуту раньше по UNIX (вычтенное время)
    $date_minus_5minutes_UNIX = strtotime("-5 minutes"); 
    
    // Проверка условия даты и времени, статуса в сети. 
    // Если значение активности пользователя больше чем одна 1 минута от вычтенного времени и меньше чем текущее время, а статус равен 1 (значит в сети)
    if( ($date_activitystatus_UNIX >= $date_minus_5minutes_UNIX) AND ($date_activitystatus_UNIX <= $date_now_UNIX) AND ($onlinestatus == 1) ) {
        return 'Сейчас в сети';
    } else {

        // Установка даты и времени пользователя (поле date_activitystatus) в формат и приведение в часовую зону пользователя 
        $date_status = date("Y-m-d H:i:s", strtotime($date));
        // time_elapsed_string() - функция отображающая дату и время активности пользователя как в соц. сетях (минут/часов/дней назад)
        return 'Был(-a) в сети, ' . onlinestatus_date_time_elapsed($date_status);
    }
}

function time_elapsed($datetime, $timezone, $n_time = 1, $full = false) {
  
    // Установка временной зоны пользователя
    date_default_timezone_set($timezone);

    // Дата и время из базы данных MySQL
    $ago = new DateTime($datetime);

    // Установка дат
    $now = new DateTime;

    // Разница между текущей датой и датой параметра $datetime
    $diff = $now->diff($ago);

    // Округляет дробь в меньшую сторону
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    
    // Массив со значениями
    $string = array(
                    'y' => 'г.',
                    'm' => 'мес.',
                    'w' => 'нед.',
                    'd' => 'дн.',
                    'h' => 'ч.',
                    'i' => 'мин.',
                    's' => 'сек.');
    foreach ($string as $k => &$v) {
        if ($diff->$k) 
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        else
            unset($string[$k]);
    }
    // Если True, то показывает промежуток полностью до секунды
    if (!$full) $string = array_slice($string, 0, $n_time);
    return $string ? implode(' ', $string) . ' назад' : 'сейчас';
}

// Вычисление возраста от даты рождения
function dateofbirth($datetime) {

    // Сегодняшняя дата
    $today = new DateTime();

    // Дата рождения
    $birthdate_in_DB = new DateTime($datetime);
    
    // Вычисление даты рождения
    $interval = $today->diff($birthdate_in_DB);
    
    // Возраст пользователя
    $age = $interval->format('%y');

    // 
    // Далее ПРАВИЛЬНОЕ написания год/лет/года и RETURN
    // 

    // Получение последней цифры от возраста с помощью операторп деления с остатком ( % )
    // Пример: Число 29 даст цифру 9
    $last_number_age = $age % 10;

    // Условие с иключение для возрастов 11, 12, 13, 14
    // Должны записываться как 11 лет, 12 лет 
    if ($age >= 11 && $age <= 14) {
        return $age . ' лет';
    }

    // Основное условие для возрастов
    // Года, оканчивающиеся на цифру 1 (1, 21, 31 и т.д.), должны записываться как год
    if($last_number_age == 1) {
        return $age . ' год';
    // Года, оканчивающиеся на цифры 2, 3, 4 (2, 22, 3, 23, 4, 24 и т.д.), должны записываться как года
    } elseif ($last_number_age >= 2 && $last_number_age <= 4) {
        return $age . ' года';
    // Все остальные возраста должны записываться со словом лет: 5 лет, 10 лет, 69 лет и т.д.
    } else {
        return $age . ' лет';
    }
}

// Определение пола пользователя
function gender_user(string $gender, int $declension = 1) {
    // Если второй тип склонения
    if($declension == 2) {
        if($gender == 'male') return 'Мужчина';
        else return 'Женщина';
    }

    // Если первый тип склонения
    if($declension == 1) {
        if($gender == 'male') return 'Мужской';
        else return 'Женский';
    }
}

// Генератор случайных символов для создания уникальных строк
function generateRandomString(int $length = 8) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

// Генератор случайных чисел для создания уникальных строк
function generateRandomInteger(int $length = 6) {
    return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
}