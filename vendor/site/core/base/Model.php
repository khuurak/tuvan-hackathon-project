<?php

namespace siteCore\base;

use siteCore\Db;

abstract class Model
{
	public $attributes = [];
	public $errors = [];
	public $rules = [];

	public function __construct()
	{
		Db::instance();
	}

	// Метод добавления данных в таблицу
	public function save($table)
	{
		// Обход проверки в именах таблиц с нижним подчеркиванием.
		\R::ext('xdispense', function( $type ){
			return \R::getRedBean()->dispense( $type );
		});

		$tbl = \R::xdispense($table);
		foreach($this->attributes as $name => $value) {
			$tbl->$name = $value;
		}
		return \R::store($tbl);
	}

	// Метод обновления данных в таблицу
	public function update($table, $id)
	{
		$tbl = \R::load($table, $id);
		foreach($this->attributes as $name => $value) {
			$tbl->$name = $value;
		}
		return \R::store($tbl);
	}
	
	// Метод загрузки данных из формы 
	public function load($data) 
	{
		foreach($this->attributes as $name => $value) {
			if(isset($data[$name])) {
				$this->attributes[$name] = $data[$name];
			}
		}
	}
}