<?php

namespace siteCore\base;

class View
{
	public $route;
	public $controller;
	public $model;
	public $view;
	public $prefix;
	public $layout;
	public $data = [];
	public $meta = [];
	public $javascript_links = [];
	public $css_links = [];
	public $timezone_mysql;
	public $timezone_php;

	public function __construct($route, $layout = '', $view = '', $meta, $javascript_links, $css_links, $timezone_mysql, $timezone_php)
	{
		$this->route = $route;
		$this->controller = $route['controller'];
		$this->view = $view;
		$this->model = $route['controller'];
		$this->meta = $meta;
		$this->javascript_links = $javascript_links;  
		$this->css_links = $css_links;
		$this->timezone_mysql = $timezone_mysql;
		$this->timezone_php = $timezone_php;

		// Если prefix (из config/routes.php) не пустой, то убираем заменяем слеши для того, чтобы был коректен на сервере Linux
		// Иначе сохраняем как есть
		if(!empty($route['prefix'])) {
			$this->prefix = str_replace('\\', '/', $route['prefix']);
		} else {
			$this->prefix = $route['prefix'];
		}

		// Приведение шаблона
		if($layout === false){
			$this->layout = false;
		} else {
			$this->layout = $layout ?: LAYOUT;
		}
	}

	// Отрисовака представления
	public function render($data)
	{
		// переданные данные массива из контроллера определяем как переменные для представления
		if( is_array($data) ) extract($data);

		
		// Приведение файла вида 
		$viewFile = APP . "/views/{$this->prefix}{$this->controller}/{$this->view}.php";

		// Если файл вида существует, то содержимое файла вида с помощью буфера ob_start() вырезаем 
		// и вставляем в переменную $content. Вывод переменной осуществляется в шаблоне в папке app/views/layouts
		if(is_file($viewFile)) {
			ob_start();
			require_once $viewFile;
			$content = ob_get_clean();
		} else {
			throw new \Exception("Не найден вид {$viewFile}", 500);	
		}
		
		if(false !== $this->layout) {
			$layoutFile = APP . "/views/layouts/{$this->layout}.php";
			
			if(is_file($layoutFile)) {
				require_once $layoutFile;
			} else {
				throw new \Exception("Не найден шаблон {$this->layout} в папке views/layouts", 500);	
			}
		}
	}

	// Отправка метаданных
	public function getMeta()
	{	
		$output = (!empty($this->meta['title'])) ? '<title>'.$this->meta['title'] .'</title>' . PHP_EOL : '<title>'. NAME_SITE .'</title>' . PHP_EOL ;
		$output .= (!empty($this->meta['description'])) ? '<meta name="description" content="'. $this->meta['description'] .'">' . PHP_EOL : '';
		$output .= (!empty($this->meta['keywords'])) ? '<meta name="keywords" content="'. $this->meta['keywords'] .'">' . PHP_EOL : '';

		return $output;
	}

	// Отправка скриптов
	public function getJavascript()
	{
		// Если не пусто, то отправляем в вид
		// Иначе оправляем false значение
		if( !empty($this->javascript_links) ) {
			$output = '';
			foreach($this->javascript_links as $v) {
				$output .= '<script src="'. $v .'"></script>' . PHP_EOL;
			}

			return $output;
		} else {
			return false;
		}
	}

	// Отправка стилей
	public function getCss()
	{
		// Если не пусто, то отправляем в вид
		// Иначе оправляем false значение
		if( !empty($this->css_links) ) {
			$output = '';
			foreach($this->css_links as $v) {
				$output .= '<link rel="stylesheet" href="'. $v .'">' . PHP_EOL;
			}

			return $output;
		} else {
			return false;
		}
	}

	public function getTimezoneUser()
	{
		return $this->timezone_php;
	}

}