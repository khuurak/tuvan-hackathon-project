<?php

// Базовый контроллер

namespace siteCore\base;

abstract class Controller
{
	public $route;
	public $controller;
	public $model;
	public $view;
	public $prefix;
	public $layout;
	public $data = [];
	public $meta = ['title' => '', 'description' => '', 'keywords' => '']; 
	public $javascript_links = [];
	public $css_links = [];
	public $timezone_mysql = '+07:00'; // Красноярский (Тыва) часовой пояс по-умолчанию для запросов в базу данных MySQL. Пример: SET time_zone = "+00:00"
	public $timezone_php = 'Asia/Krasnoyarsk'; // Красноярский (Тыва) часовой пояс по-умолчанию для функций PHP. Пример: SET time_zone = "+00:00"

	public function __construct($route)
	{
		$this->route = $route;
		$this->controller = $route['controller'];
		$this->model = $route['controller'];
		$this->view = $route['action'];
		$this->prefix = $route['prefix'];

		// ГЛОБАЛЬНЫЕ УСТАНОВКИ
		// Если существуют COOKIE
		if( isset($_COOKIE['geo-timezone-mysql']) ) {
			$this->timezone_mysql = $_COOKIE['geo-timezone-mysql'];
		}
		if( !empty($_COOKIE['geo-timezone-php']) ) {
			$this->timezone_php = $_COOKIE['geo-timezone-php'];
		}

		// Если существует сессия авторазации
		if( isset($_SESSION['auth']) ){

		}
	}

	public function getView()
	{
		$viewObject = new View($this->route, $this->layout, $this->view, $this->meta, $this->javascript_links, $this->css_links, $this->timezone_mysql, $this->timezone_php);
		$viewObject->render($this->data); 
	}

	// В массив data[] передать данные
	public function set($data)
	{
		$this->data = $data;
	}

	// Для метаданных
	public function setMeta($title = '', $description = '', $keywords = '')
	{
		$this->meta['title'] = $title;
		$this->meta['description'] = $description;
		$this->meta['keywords'] = $keywords;
	}

	// Для скриптов
	public function setJavascript(array $link)
	{	
		// Если свойсто (массив) не пустой, то объединяем эти массивы и заново присваиваем свойству
		// Иначе пустой, то присваиваем пустому свойству новые ссылки
		if( !empty($this->javascript_links) ) {
			$this->javascript_links = array_merge($this->javascript_links, $link);
		} else {
			$this->javascript_links = $link;
		}
	}

	// Для скриптов
	public function setCss(array $link)
	{	
		// Если свойсто (массив) не пустой, то объединяем эти массивы и заново присваиваем свойству
		// Иначе пустой, то присваиваем пустому свойству новые ссылки
		if( !empty($this->css_links) ) {
			$this->css_links = array_merge($this->css_links, $link);
		} else {
			$this->css_links = $link;
		}
	}

	// Проверка на ajax запрос
	public function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

	// CURL-запрос на звонок SMS.ru для верификации номера телефона
	public function verifyCallSmsRU(string $phone) 
	{
		$ch = curl_init("https://sms.ru/code/call");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"phone" => $phone, // номер телефона пользователя
			"ip" => $_SERVER["REMOTE_ADDR"], // ip адрес пользователя
			"api_id" => SMSRU_API_ID // API ID sms.ru, константа берется из init.php
		)));
		$body = curl_exec($ch);
		curl_close($ch);

		return json_decode($body);
    }
}

