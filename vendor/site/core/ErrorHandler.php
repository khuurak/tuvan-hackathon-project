<?php

// Обработка ошибок. Обрабатыавет только исключения

namespace siteCore;

class ErrorHandler
{
	public function __construct()
	{
		// Состоние константы debug в public/init.php
		if(DEBUG) {
			error_reporting(-1);
		} else {
			error_reporting(0);
		}

		// Исключение передает в метод exceptionHandler()
		set_exception_handler([$this, 'exceptionHandler']);
	}

	// Метод который обрабатывает перехваченное исключение
	public function exceptionHandler($e)
	{
		$this->logErrors( $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode() );
		$this->displayError( 'Исключение', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode() );
	}

	// Метод логирования ошибки
	protected function logErrors($message  = '', $file = '', $line = '', $responce = '')
	{
		error_log("[" . date('Y-m-d H:i:s') . "] Ошибка: {$message} | Файл: {$file} | Строка {$line}. Код ошибки: {$responce} | Ссылка: ". $_SERVER['REQUEST_URI'] . "\n===\n", 3, ROOT.'/tmp/errors.log');
	}

	// Метод вывода ошибок (номер ошибки - строка ошибки - файл - линия - код ошибки)
	protected function displayError($no, $str, $file, $line, $responce = 404)
	{
		// Отправить заголовок
		http_response_code($responce);

		// Подключаем шаблон ошибки
		// Если код 404 и DEBUG отключен (режим продакшна, т.е. значение равен 0)
		if($responce == 404 && !DEBUG) {
			require WWW . '/errors/404.php';
			die;
		}
		
		// DEBUG включен (режим разработки, т.е. значение равен 1)
		if(DEBUG) {
			require WWW . '/errors/development.php';
		} else {
			require WWW . '/errors/production.php';
		}
		die;
	}
}