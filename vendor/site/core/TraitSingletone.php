<?php

// Создаем приватное свойство $instance. Это свойства заполняем объектом если его там нет.

namespace siteCore;

trait TraitSingletone
{
	private static $instance;

	public static function instance()
	{
		if(self::$instance === null){
			self::$instance = new self;
		}

		return self::$instance;
	}
}