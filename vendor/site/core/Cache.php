<?php

namespace siteCore;

class Cache 
{
	use TraitSingletone;

	// Записываем в кеш
	public function set($key, $data, $seconds = 3600)
	{
		if($seconds) {
			$content['data'] = $data;
			$content['end_time'] = time() + $seconds;

			// Запись в кеш
			if( file_put_contents(CACHE . '/' . md5($key) . '.txt', serialize($content)) ) {
				return true;
			}

			return false;

		}
	}

	// Получаем из кеш
	public function get($key)
	{
		$file = CACHE . '/' . md5($key) . '.txt';

		if(file_exists($file)) {
			$content = unserialize(file_get_contents($file));

			if(time() <= $content['end_time']) {
				return $content;
			}

			unlink($file);
		}

		return false;
	}

	// Удаляем кеш
	public function delete($key)
	{
		$file = CACHE . '/' . md5($key) . '.txt';

		if(file_exists($file)) {
			unlink($file);
		}
	}
}