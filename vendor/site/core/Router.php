<?php

namespace siteCore;

class Router
{	
	// Свойство, которое должно хранить имеющиеся маршруты
	protected static $routes = [];

	// Свойство, которое должно хранить текущий массив запрошенного в браузере
	protected static $route = [];

	// Метод записи правила маршрутов из config/routes.php
	public static function add($regexp, $route = [])
	{
		self::$routes[$regexp] = $route;
	}

	// Метод получения маршрутов (для ТЕСТА вывода)
	public static function getRoutes()
	{
		return self::$routes;
	}

	// Метод получения текущего маршрута (для ТЕСТА вывода)
	public static function getRoute()
	{
		return self::$route;
	}

	// Метод принимаюцщий url адрес 
	public static function dispatch($url)
	{
		// С помощью метода вырезаем из браузерной строки GET параметр начинающий символом "?"
		$url = self::removeQueryString($url);

		// Если метод matchRoute() вернул TRUE
		if(self::matchRoute($url)) {

			// Приведение к виду контроллера с постфиксом Controller 
			// (постфикс нужен для того, чтобы отличать от модели)
			// Пример: app\controllers\MainController - при главной странице
			 $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';

			// Проверка на существование такого класса, а если нет, то отправим ошибку
			if(class_exists($controller)){

				// Приведение объекта класса
				// Параметр $route уходит в главный контроллер AppController, который в свою очередь наследуется в всех контроллерах
				$controllerObject = new $controller(self::$route);
				
				// Приведение экшена (метода контроллера) с постфиксом Action 
				// (постфикс нужен для того, чтобы отличать и не вызывать служебные экшены в конроллере)
				$action = self::lowerCamelCase(self::$route['action']) . 'Action';

				// Проверка на существование такого метода (экшена), а если нет, то отправим ошибку
				if(method_exists($controllerObject, $action)) {
					$controllerObject->$action();
					$controllerObject->getView();
				} else {
					throw new \Exception("Метод <b>$controller::$action</b> не найден", 404);
				}

			} else {
				throw new \Exception("Контроллер <b>$controller</b> не найден", 404);
			}
		} else {
			throw new \Exception("Страница не найдена", 404);

		}

	}

	// Метод принимающий url адрес запрошенный и ищущий соотвествие в таблице маршрутов
	public static function matchRoute($url)
	{
		// Перебор массива маршрутов из свойства $routes[]
		foreach (self::$routes as $pattern => $route) {

			// Ищет совпадания с шаблоном маршрутов из config/routes.php по введуному пользователем URL, 
			// а затем сохраняет в новый массив  $matches
			if(preg_match("#{$pattern}#", $url, $matches)) {

				// Перебором массива $matches (берутся только текстовые ключи) присваивается
				// свойству $route{}, которое должно хранить текущий массив запрошенного в браузере
				foreach ($matches as $key => $value) {
					if(is_string($key)) {
						$route[$key] = $value;
					}
				}
				
				// Если ключ action пустой, то присваивается index по-умолчанию 
				if(empty($route['action'])) {
					$route['action'] = 'index';
				}

				// Если ключа prefix нет, то присваивается ничего по-умолчанию,
				// а если есть ключ prefix, то в конце добавляем слеш "\"
				if(!isset($route['prefix'])) {
					$route['prefix'] = '';
				} else {
					$route['prefix'] .= '\\';
				}

				// Контроллер приводим в CamelCase
				$route['controller'] = self::upperCamelCase($route['controller']);
		
				self::$route = $route;

				return true;
			}
		}

		return false;
	}

	// Метод приводит к наименованию CamelCase (для контроллеров)
	protected static function upperCamelCase($name)
	{
		return str_replace(' ', '', ucwords(str_replace('-',' ', $name)));
	}

	// Метод приводит к наименованию camelCase (для экшенов)
	protected static function lowerCamelCase($name)
	{
		return lcfirst(self::upperCamelCase($name));
	}

	// Вырезает GET параметры
	protected static function removeQueryString($url)
	{
		if($url) {
			$params = explode('&', $url, 2);
			if(false === strpos($params[0], '=')) {
				return rtrim($params[0], '/');
			} else {
				return '';
			}
		}
	}	

}