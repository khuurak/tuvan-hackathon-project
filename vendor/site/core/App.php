<?php

// Главный класс, который подключается в public/index.php

namespace siteCore;

class App
{
	// Получение доступа к параметрам 
	// (контейнер приложения где можно положить различные свойства и получить эти свойства)
	public static $app;

	public function __construct()
	{
		// Получаем строку запроса. С помощью trim() обрезаем концевую слеш.
		// Значение: "domain.ru/GET/GET/" --> "domain.ru/GET/GET"
		$query = trim($_SERVER['QUERY_STRING'], '/');

		// Проверка статуса сессии
		// Если статус не активен или id равен пустому значению, то запустить сессию
		if (session_status() !== PHP_SESSION_ACTIVE || session_id() === ""){
			// Cтарт сессии для авторизации, админки
			session_start(); 
		}

		// Класс реестров
		self::$app = Registry::instance();
		
		// Вызов параметра 
		$this->getParams();

		// Класс ошибки
		new ErrorHandler();

		// Маршрутизатор
		Router::dispatch($query);

	}

	// Получаем все параметры (из массива) из файла
	protected function getParams()
	{
		$params = require_once CONFIG . '/params.php';
		if(!empty($params)){
			foreach ($params as $key => $value) {
				// Все параметры кладем в контейнер (ключ и значение)
				self::$app->setProperty($key, $value);
			}
		}
	}

	
}