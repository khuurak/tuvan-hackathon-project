<?php

namespace siteCore;

class Db
{
	use TraitSingletone;

	protected function __construct()
	{
		$db = require_once CONFIG . '/configdb.php';

		// Создание короткого алиаса
		class_alias('\RedBeanPHP\R', '\R');
 
		// Подключение к БД
		\R::setup($db['dsn'], $db['user'], $db['password']);

		// Проверка соединения с БД
		if( !\R::testConnection() ) {
			throw new \Exception("Нет соединения с БД", 500);
		}

		// Установка временной зоны глобально
		// \R::exec("SET GLOBAL time_zone = '+00:00'");

		// Заморозить изменения БД (не разрешать изменения автоматически)
		\R::freeze(true);

		// Включаем дебаг
		if(DEBUG) {
			\R::debug(true, 1);
		}
	}
}