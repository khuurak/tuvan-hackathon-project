<?php

// Реестр

namespace siteCore;

class Registry
{
	// Вставка трейта
	use TraitSingletone;

	// Контейнер (защищенное свойство) для наполнения свойствами
	protected static $properties = [];

	// set - Будет класть в контейнер свойства. $name = это ключ, $value = это значение
	public function setProperty($name, $value)
	{
		// Обращаемся к текущему свойству (контейнер) и по ключу наполняем
		self::$properties[$name] = $value;
	}

	// get - Будет получать из контейнера свойство
	public function getProperty($name)
	{
		// Если свойство существует, то вернуть. Если нет, то null
		if(isset(self::$properties[$name])) {
			return self::$properties[$name];
		}

		return null;
	}

	// Для возвращения всех свойств
	public function getProperties()
	{
		return self::$properties;
	}



}