<?php 

namespace app\models;

class RolesModel extends AppModel 
{
  public function getAllOrOne(string $id = null) {
    $query = "SELECT 
                id,
                name,
                descr
              FROM roles";

    $result = '';

    if (!is_null($id)) {
      $query.=" WHERE id='$id'";
      $result = \R::getRow($query);
    } else $result = \R::getAll($query);
    // Отправить и получить ответ запроса
    return $result;
  }

  public function createOne(string $name, string $descr) {
    $query = "INSERT INTO roles (
      name,
      descr
    ) VALUES (
        '{$name}',
        '{$descr}'
    )";
    return \R::exec($query);
  }

  public function updOne(string $id, string $descr) {
    $query = "UPDATE roles SET descr='$descr' WHERE id='$id'";
    return \R::exec($query);
  }

  public function delOne(string $id) {
    $query = "DELETE FROM roles WHERE id='$id'";
    return \R::exec($query);
  }
}