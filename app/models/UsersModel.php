<?php 

namespace app\models;

use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;

class UsersModel extends AppModel 
{
  public function getAllOrOne(string $uid = null) {
    $query = "SELECT 
                uid AS 'identifier',
                fam,
                im,
                phone,
                email,
                role,
                password
                  FROM users";

    $result = '';

    if (!is_null($uid)) {
      $query.=" WHERE uid='$uid'";
      $result = \R::getRow($query);
    } else $result = \R::getAll($query);
    // Отправить и получить ответ запроса
    return $result;
  }

  public function getOneIsEmail(string $email = null) {
    $query = "SELECT 
                uid AS 'identifier',
                fam,
                im,
                phone,
                email,
                role,
                password
                  FROM users";

    $result = '';

    if (!is_null($email)) {
      $query.=" WHERE email='$email'";
      $result = \R::getRow($query);
    } else $result = \R::getAll($query);
    // Отправить и получить ответ запроса
    return $result;
  }

  public function createOne(string $fam = null, string $im = null, string $phone = null, string $email, string $password) {
    $client = new Client();

    $uid = $client->generateId($size = 20);
    $password = password_hash($password, PASSWORD_DEFAULT);

    $query = "INSERT INTO users (
      uid,
      fam,
      im,
      phone,
      email,
      password
    ) VALUES (
        '{$uid}',
        '{$fam}',
        '{$im}',
        '{$phone}',
        '{$email}',
        '{$password}'
    )";
    return \R::exec($query);

    // Отправить и получить ответ запроса
    return \R::getRow($query);
  }

  public function updOne(string $uid, string $fam, string $im, string $phone) {
    $query = "UPDATE users SET fam='$fam', im='$im', phone='$phone' WHERE uid='$uid'";
    return \R::exec($query);
  }

  public function delOne(string $uid) {
    $query = "DELETE FROM users WHERE uid='$uid'";
    return \R::exec($query);
  }
}