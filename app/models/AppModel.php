<?php 

namespace app\models;

use siteCore\base\Model;

use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;

class AppModel extends Model 
{

        public function getUser(string $identifier) {
            // Проверка пользователя на наличие в базе
                // Строка запроса
                $query = "SELECT 
                            uid AS 'identifier',
                            fam,
                            im,
                            phone,
                            email,
                            password
                                FROM users WHERE uid = ?"; 
    
                // Отправить и получить ответ запроса
                $user = \R::getRow($query, [ $identifier ]);
        }


        // Метод входа пользователя
        public function signin(string $email, string $password)
        {
            // Если введенные почта и пароль существуют и они не пустые
            if($email && $password) {
    
                // Проверка пользователя на наличие в базе
                // Строка запроса
                $query = "SELECT 
                            uid AS 'identifier',
                            fam,
                            im,
                            phone,
                            email,
                        password
                            FROM users WHERE email = ?"; 
    
                // Отправить и получить ответ запроса
                $user = \R::getRow($query, [ $email ]);
    
                // Если существует пользователь, то проверяем пароль
                if($user) {
                    // Проверка пароля на совпадание
                    if(password_verify($password, $user['password'])) {
    
                        // Если пароль совпадет, то через цикл сохраняем 
                        // значения полей в сессию авторизации, но при этом исключив сам пароль
                        foreach($user as $k => $v) {
                            if($k != 'password') $_SESSION['auth'][$k] = $v; 
                        }
    
                        return true;
                        
                    } else {
                        return false;
                    }
                }
            }
            
            // Если введенные почта и пароль не существуют, то отправляем false
            return false;
        }
    
        // Метод регистрации
        public function signup(string $phone, string $email, string $password)
        {   
            // Хеширование пароля
            $password = password_hash($password, PASSWORD_DEFAULT);
            
            // Если данные не пусты, то сохраняем в строку запросу
            // Иначе, отправляем false
            if( $phone && $email && $password ) {
                $client = new Client();
                
                $uid = $client->generateId($size = 20);
            
                $query = "INSERT INTO users (
                  uid,
                  fam,
                  im,
                  phone,
                  email,
                  password
                ) VALUES (
                    '{$uid}',
                    'Фамилия',
                    'Имя',
                    '{$phone}',
                    '{$email}',
                    '{$password}'
                )";
                return \R::exec($query);
    
            } else {
                return false;
            }
        }
}