<?php 

namespace app\models;

use siteCore\base\Model;

use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;

class GrantsModel extends Model 
{
  public function getAllOrOne(string $code = null, string $my = null) {
    // Строка запроса
    $result = '';

    if (!is_null($code)) {
      $query = "SELECT 
        gr.id,
        gr.code,
        gr.name,
        gr.descr_short,
        gr.descr_full,
        gr.location,
        gr.date_create,
        DATE_FORMAT(gr.date_create, '%d %M %Y %H:%s') AS 'date_create_format', 
        gr.date_update,
        gr.owner_uid,
        gr.contractor_uid,
        gr.status,
        gr.finance_requested,
        gr.finance_cofinancing,
        gr.finance_general_budget,
        gr.info_goals,
        gr.info_tasks,
        gr.info_social_significance,
        gr.info_geography,
        gr.info_target_groups,
        gr.date_start,
        DATE_FORMAT(gr.date_start, '%d %M %Y') AS 'date_start_format', 
        gr.date_finished,
        DATE_FORMAT(gr.date_finished, '%d %M %Y') AS 'date_finished_format', 
        gr.grantDirection_id,
        gr.contest_id,
        
        own.id as owner_id,
        own.uid as owner_uid,
        own.fam as owner_fam,
        own.im as owner_im,
        own.phone as owner_phone,
        own.email as owner_email
      FROM grants gr
        LEFT JOIN users own ON gr.owner_uid = own.uid
      WHERE code='$code'";

      $result = \R::getRow($query);
    } else {
      $query = "SELECT
        gr.id,
        gr.code,
        gr.name,
        gr.descr_short,
        gr.descr_full,
        gr.location,
        gr.date_create,
        DATE_FORMAT(gr.date_create, '%d %M %Y %H:%s') AS 'date_create_format', 
        gr.date_update,
        gr.owner_uid,
        gr.contractor_uid,
        gr.status,
        gr.finance_requested,
        gr.finance_cofinancing,
        gr.finance_general_budget,
        gr.info_goals,
        gr.info_tasks,
        gr.info_social_significance,
        gr.info_geography,
        gr.info_target_groups,
        gr.date_start,
        DATE_FORMAT(gr.date_start, '%d %M %Y') AS 'date_start_format', 
        gr.date_finished,
        DATE_FORMAT(gr.date_finished, '%d %M %Y') AS 'date_finished_format', 
        gr.grantDirection_id,
        gr.contest_id,
        
        own.id as owner_id,
        own.uid as owner_uid,
        own.fam as owner_fam,
        own.im as owner_im,
        own.phone as owner_phone,
        own.email as owner_email
      FROM grants gr
        LEFT JOIN users own ON gr.owner_uid = own.uid
      ";

      if (!is_null($my)) $query.=" WHERE owner_uid='${my}'";

      $result = \R::getAll($query);
    }

    // Отправить и получить ответ запроса
    return $result;
  }

  public function createOne(string $date_finished = null,
    string $date_start = null,
    string $descr_full = null,
    string $descr_short = null,
    int $finance_cofinancing,
    int $finance_general_budget,
    int $finance_requested,
    string $info_geography,
    string $info_goals,
    string $info_social_significance,
    string $info_target_groups,
    string $info_tasks,
    string $name,
    int $grantDirection_id,
    int $contest_id
  ) {

    $client = new Client();

    $code = $client->generateId($size = 20);

    $owner = $_SESSION['auth']['identifier'];

    $date_start__sql_format = $date_start ? date('Y-m-d H:i:s', strtotime($date_start)) : null;
    $date_finished__sql_format = $date_finished ? date('Y-m-d H:i:s', strtotime($date_finished)) : null;

    $str1 = $date_start ? "date_start," : "";
    $str1.= $date_finished ? "date_finished," : "";

    $str2 = $date_start ? "'{$date_start__sql_format}'," : "";
    $str2.= $date_finished ? "'{$date_finished__sql_format}'," : "";

    $query = "INSERT INTO grants (
      name,
      info_tasks,
      info_target_groups,
      info_social_significance,
      info_goals,
      info_geography,
      finance_requested,
      finance_general_budget,
      finance_cofinancing,
      descr_short,
      descr_full,";
    $query.= $str1;
    $query.= "code,
      owner_uid,
      grantDirection_id,
      contest_id
    ) VALUES (
        '{$name}',
        '{$info_tasks}',
        '{$info_target_groups}',
        '{$info_social_significance}',
        '{$info_goals}',
        '{$info_geography}',
        {$finance_requested},
        {$finance_general_budget},
        {$finance_cofinancing},
        '{$descr_short}',
        '{$descr_full}',";
    $query.= $str2;
    $query.= "'{$code}',
        '{$owner}',
        {$grantDirection_id},
        {$contest_id}
    )";

    // echo $query;

    return \R::exec($query);
  }

  public function updOne(int $id, string $name) {
    $query = "UPDATE grants SET name='$name' WHERE id='$id'";
    return \R::exec($query);
  }

  public function delOne(int $id) {
    $query = "DELETE FROM grants WHERE id='$id'";
    return \R::exec($query);
  }
}