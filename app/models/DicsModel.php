<?php 

namespace app\models;

use siteCore\base\Model;

class DicsModel extends Model 
{
  public function getDics(string $code = null) {
    // Строка запроса
    $query = "SELECT 
      id,
      name,
      code,
      descr
        FROM dics";

    $result = '';

    if (!is_null($code)) {
      $query.=" WHERE code='$code'";
      $result = \R::getRow($query);
    } else $result = \R::getAll($query);

    // Отправить и получить ответ запроса
    return $result;
  }

  public function createDic(string $name, string $code, string $descr) {
    $query = "INSERT INTO dics (
      name,
      code,
      descr
    ) VALUES (
        '{$name}',
        '{$code}',
        '{$descr}'
    )";
    return \R::exec($query);
  }

  public function updDic(string $code, string $name, string $descr) {
    $query = "UPDATE dics SET name='$name', descr='$descr' WHERE code='$code'";
    return \R::exec($query);
  }

  public function delDic(string $code) {
    $query = "DELETE FROM dics WHERE code='$code'";
    return \R::exec($query);
  }
}