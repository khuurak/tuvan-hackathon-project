<?php 

namespace app\models;

use siteCore\base\Model;

class DicsBodyModel extends Model 
{
  public function getAllOrOne(string $dic_code, string $id = null) {
    // Строка запроса
    $query = "SELECT 
      id,
      name,
      descr
        FROM dics_body WHERE dics_code='$dic_code'";

    $result = '';

    if (!is_null($id)) {
      $query = $query." AND id=?";
      $result = \R::getRow($query, [$id]);
    } else {
      $result = \R::getAll($query);
    }

    // Отправить и получить ответ запроса
    return $result;
  }

  public function createOne(string $dicCode, string $name, string $descr = '') {
    $query = "INSERT INTO dics_body (
      name,
      descr,
      dics_code
    ) VALUES (
        '{$name}',
        '{$descr}',
        '{$dicCode}'
    )";
    return \R::exec($query);
  }

  public function updOne(string $id, string $name = null, string $descr = null) {
    $query = "UPDATE dics_body SET name='$name', descr='$descr' WHERE id='$id'";
    return \R::exec($query);
  }

  public function delOne(string $id) {
    $query = "DELETE FROM dics_body WHERE id='$id'";
    return \R::exec($query);
  }
}