<?php
// debug($_POST);
?>

<main class="form-signin w-100 m-auto">
  <form method="POST">
    <h1 class="h3 mb-3 fw-normal">Пожалуйста войдите</h1>

    <div class="form-floating">
      <input type="email" name="email" class="form-control" id="email" placeholder="имя@пример.com">
      <label for="email">Адрес электронной почты</label>
    </div>
    <div class="form-floating">
      <input type="password" name="password" class="form-control" id="password" placeholder="Пароль">
      <label for="password">Пароль</label>
    </div>

    <button class="w-100 btn btn-lg btn-primary" type="submit">Войти</button>
  </form>
</main>