<?php
// debug($_POST);
?>

<main class="form-signin w-100 m-auto">
  <form method="POST">

    <div class="mb-3">
      <label for="phone" class="form-label">Телефон</label>
      <input type="text" name="phone" class="form-control" id="phone">
    </div>

    <div class="mb-3">
      <label for="email" class="form-label">Электроннная почта</label>
      <input type="email" name="email" class="form-control" id="email">
    </div>

    <div class="mb-3">
      <label for="password" class="form-label">Пароль</label>
      <input type="password" name="password" class="form-control" id="password">
    </div>

    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
  </form>
</main>