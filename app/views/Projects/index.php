<?php
// debug($projects);
?>

<section class="our-faq bgc-fa mt50">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-xl-3 dn-smd">
					<div class="faq_search_widget mb30">
						<h4 class="fz20 mb15">Поиск по названию проекта</h4>
						<div class="input-group mb-3">
							<input type="text" class="form-control" placeholder="Введите название" aria-label="Recipient's username" aria-describedby="button-addon2">
							<div class="input-group-append">
						    	<button class="btn btn-outline-secondary" type="button" id="button-addon2"><span class="flaticon-search"></span></button>
							</div>
						</div>
					</div>

					<div class="faq_search_widget mb30">
						<h4 class="fz20 mb15">Населенный пункт</h4>
						<div class="input-group mb-3">
							<input type="text" class="form-control" placeholder="Введите название" aria-label="Recipient's username" aria-describedby="button-addon2">
							<div class="input-group-append">
						    	<button class="btn btn-outline-secondary" type="button" id="button-addon3"><span class="flaticon-location-pin"></span></button>
							</div>
						</div>
					</div>
					<div class="faq_search_widget mb30">
						<h4 class="fz20 mb15">Категория</h4>
						<div class="candidate_revew_select">
							<select class="selectpicker w100 show-tick">
								<option>Категория 1</option>
								<option>Категория 2</option>
								<option>Категория 3</option>
								<option>Категория 4</option>
							</select>
						</div>
					</div>
					<div class="cl_latest_activity mb30">
						<h4 class="fz20 mb15">По дате публикации</h4>
						<div class="ui_kit_radiobox">

							<div class="radio">
								<input id="radio_two" name="radio" type="radio">
								<label for="radio_two"><span class="radio-label"></span> За сутки</label>
							</div>
							<div class="radio">
								<input id="radio_three" name="radio" type="radio">
								<label for="radio_three"><span class="radio-label"></span> За 7 дней</label>
							</div>
							<div class="radio">
								<input id="radio_four" name="radio" type="radio">
								<label for="radio_four"><span class="radio-label"></span> За 14 дней</label>
							</div>
							<div class="radio">
								<input id="radio_five" name="radio" type="radio">
								<label for="radio_five"><span class="radio-label"></span> За 30 дней</label>
							</div>
						</div>
					</div>

				</div>


				<div class="col-md-12 col-lg-9 col-xl-9">
					<div class="row">
						<div class="col-sm-12 col-lg-6">
							<div class="candidate_job_alart_btn">
								<h3 class="fz20 mb15">Всего проектов: <?=count($projects)?></h3>
								<button class="btn btn-thm btns dn db-991 float-right">Фильтр</button>
							</div>
						</div>
						<div class="col-sm-12 col-lg-6">
							<!-- <div class="candidate_revew_select text-right mt50 mt10-smd">
								<ul>
									<li class="list-inline-item">Сортировка:</li>
									<li class="list-inline-item">
										<select class="selectpicker show-tick">
											<option>Новые</option>
											<option>Старые</option>
										</select>
									</li>
								</ul>
							</div> -->
							<div class="content_details">
								<div class="details">
									<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Закрыть</span><i>×</i></a>
									<div class="faq_search_widget mb30">
										<h4 class="fz20 mb15">Поиск по названию проекта</h4>
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Введите название" aria-label="Recipient's username" aria-describedby="button-addon2">
											<div class="input-group-append">
												<button class="btn btn-outline-secondary" type="button" id="button-addon2"><span class="flaticon-search"></span></button>
											</div>
										</div>
									</div>
									<div class="faq_search_widget mb30">
										<h4 class="fz20 mb15">Населенный пункт</h4>
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Введите название" aria-label="Recipient's username" aria-describedby="button-addon2">
											<div class="input-group-append">
												<button class="btn btn-outline-secondary" type="button" id="button-addon3"><span class="flaticon-location-pin"></span></button>
											</div>
										</div>
									</div>
									<div class="faq_search_widget mb30">
										<h4 class="fz20 mb15">Категория</h4>
										<div class="candidate_revew_select">
											<select class="selectpicker w100 show-tick">
												<option>Категория 1</option>
												<option>Категория 2</option>
												<option>Категория 3</option>
												<option>Категория 4</option>
											</select>
										</div>
									</div>
									<div class="cl_latest_activity mb30">
										<h4 class="fz20 mb15">По дате публикации</h4>
										<div class="ui_kit_radiobox">

											<div class="radio">
												<input id="radio_two" name="radio" type="radio">
												<label for="radio_two"><span class="radio-label"></span> За сутки</label>
											</div>
											<div class="radio">
												<input id="radio_three" name="radio" type="radio">
												<label for="radio_three"><span class="radio-label"></span> За 7 дней</label>
											</div>
											<div class="radio">
												<input id="radio_four" name="radio" type="radio">
												<label for="radio_four"><span class="radio-label"></span> За 14 дней</label>
											</div>
											<div class="radio">
												<input id="radio_five" name="radio" type="radio">
												<label for="radio_five"><span class="radio-label"></span> За 30 дней</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


                        <?php foreach($projects as $v): ?>
						<div class="col-lg-12">
							<div class="fj_post style2">
								<div class="details">
									<h3><?=$v['name']?></h3>

									<h4>Запрашиваемая сумма: <?=$v['finance_requested']?> ₽</h4>

									<div>Расходы на реализацию <?=$v['finance_general_budget']?> ₽, из них <strong>софинансирование</strong> <?=$v['finance_cofinancing']?> ₽</div>

									<ul class="featurej_post mt10">
										<li class="list-inline-item"><span class="flaticon-clock"></span> Опубликовано: <?=$v['date_create']?></li>
										<li class="list-inline-item"><span class="flaticon-location-pin"></span> Населенный пункт</li>
									</ul>
								</div>

								<a href="/projects/<?=$v['code']?>" class="btn btn-thm" style="position: relative">Посмотреть проект</a>
								<!-- <a class="favorit" href="#"><span class="flaticon-favorites"></span></a> -->
							</div>
						</div>
                        <?php endforeach; ?>


						<!-- <div class="col-lg-12">
							<div class="mbp_pagination">
								<ul class="page_navigation">
								    <li class="page-item disabled">
								    	<a class="page-link" href="#" tabindex="-1" aria-disabled="true"> <span class="flaticon-left-arrow"></span> Пред.</a>
								    </li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item active" aria-current="page">
								    	<a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
								    </li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">4</a></li>
								    <li class="page-item"><a class="page-link" href="#">5</a></li>
								    <li class="page-item"><a class="page-link" href="#">...</a></li>
								    <li class="page-item"><a class="page-link" href="#">45</a></li>
								    <li class="page-item">
								    	<a class="page-link" href="#">След. <span class="flaticon-right-arrow"></span></a>
								    </li>
								</ul>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</section>