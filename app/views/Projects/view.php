<?php
// debug($project);
?>

<section class="bgc-fa pb30 mt70 mbt45">
		<div class="container">

			<div class="row">

				<div class="col-lg-8 col-xl-8">
					<div class="candidate_personal_info style2">
						<div class="details">

							<h3><?=$project['name']?></h3>

							<ul>
								<li>Статус проекта: <span class="badge badge-primary">???</span></li>
								<li>Грантовое направление проекта: <a href="#" class="text-thm">???</a></li>
								<li>Организатор конкурса: <a href="#" class="text-thm">???</a></li>
								<li>Номер заявки: <?=$project['id']?></li>
								<li>Сроки реализации: <?=ucfirst_strtolower($project['date_start_format'])?> - <?=ucfirst_strtolower($project['date_finished_format'])?></li>
								<li>Дата подачи: <?=ucfirst_strtolower($project['date_create_format'])?></li>
							</ul>
						</div>

						<div class="row personer_information_company">
							<div class="col-12 col-md-6">
								<div class="icon text-thm"><span class="flaticon-money"></span></div>
								<div class="details">
									<p>Запрашиваемая сумма:</p>
									<p><?=$project['finance_requested']?> ₽</p>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="icon text-thm"><span class="flaticon-money"></span></div>
								<div class="details">
									<p>Софинансирование:</p>
									<p><?=$project['finance_cofinancing']?> ₽</p>
								</div>
							</div>
							<div class="col-12">
								<div class="icon text-thm"><span class="flaticon-money"></span></div>
								<div class="details">
									<p>Общая сумма расходов на реализацию проекта:</p>
									<p><?=$project['finance_general_budget']?> ₽</p>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="col-lg-4 col-xl-4">

					<div class="candidate_personal_overview style3">
						<div><strong>Руководитель проекта:</strong> <?=$project['owner_fam'] .' '. $project['owner_im']?>  </div>
						<div><strong>Телефон:</strong><?=$project['owner_phone']?></div>
						<div><strong>E-mail:</strong> <?=$project['owner_email']?></div>
					</div>

					<!-- <div class="candidate_personal_overview style3">
						<div><strong>Организация:</strong> Некоммерческая организация</div>
						<div><strong>ИНН:</strong> 123456789</div>
						<div><strong>ОГРН:</strong> 123456789101112</div>
						<div><strong>Контакты:</strong> Респ Тыва, г Кызыл, ул Кочетова, д 72, офис 426</div>
						<div><strong>Телефон:</strong> +7999-555-4433</div>
						<div><strong>E-mail:</strong> mail@mail.ru</div>
						<div><strong>Сайт:</strong> site.ru</div>
					</div> -->

				</div>
			</div>


			<div class="row">
				<div class="col-12 col-lg-8">

					<div class="candidate_about_info style2 mt10">
						<h4 class="fz20 mb10">Описание</h4>
						<p class="mb30"><?=$project['descr_full']?></p>
					</div>

					<div class="candidate_about_info style2 mt10">
						<h4 class="fz20 mb10">Цель</h4>
						<p class="mb30"><?=$project['info_goals']?></p>
					</div>

					<div class="candidate_about_info style2 mt10">
						<h4 class="fz20 mb10">Задачи</h4>
						<p class="mb30"><?=$project['info_tasks']?></p>
					</div>

					<div class="candidate_about_info style2 mt10">
						<h4 class="fz20 mb10">Обоснование социальной значимости</h4>
						<p class="mb30"><?=$project['info_social_significance']?></p>
					</div>

					<div class="candidate_about_info style2 mt10">
						<h4 class="fz20 mb10">География проекта</h4>
						<p class="mb30"><?=$project['info_geography']?></p>
					</div>

					<div class="candidate_about_info style2 mt10">
						<h4 class="fz20 mb10">Целевые группы</h4>
						<p class="mb30"><?=$project['info_target_groups']?></p>
					</div>
				</div>
			</div>

		</div>
</section>