<?php 
// Вывод модальных сообщений
// Сообщения об успехе
if( isset($_SESSION['answer']) ) {
    
    // ['answer']['title'] - для вывода заголовка
    $title = ( isset($_SESSION['answer']['title']) && !empty($_SESSION['answer']['title']) ) ? $_SESSION['answer']['title'] : '';

    // ['answer']['content'] - для вывода только текста
    // ['answer']['html'] - для вывода текста вместе с html тегами
    if( isset($_SESSION['answer']['content']) ){

        $message_show = swal2_success( $title, $_SESSION['answer']['content'] );

    } elseif (isset($_SESSION['answer']['html'])) {

        $message_show = swal2_success( $title );
        $message_show->html( $_SESSION['answer']['html'] );
    }

    // ['answer']['no-timer'] - отключение таймера закрытия окна
    // По умолчанию таймер закрытия окна установлен
    if( isset($_SESSION['answer']['no-timer']) ) {
        $message_show->timer(null);
        $message_show->timerProgressBar(null);
    } else {
        $message_show->timer(3500);
        $message_show->timerProgressBar(3500);
    }

    $message_show->showCloseButton(true);
    $message_show->showConfirmButton(false);
    echo '<script>'. $message_show .'</script>';

    // Звуковое уведомление
    // Монеты
    if( isset($_SESSION['answer']['audio-coins']) ) {
        echo '<audio id="audioID" preload><source src="/files/audio/moneysound.mp3" type="audio/mp3"></audio><script>var myaudio = document.getElementById("audioID").autoplay = true;</script>';
    }

    // Удалить сессию
    unset($_SESSION['answer']);
}

if( isset($_SESSION['answer-toast']) ) {

        if( isset($_SESSION['answer-toast']['error']) ){

            $message_show = swal2_error( $_SESSION['answer-toast']['error']['title'] );
            $message_show->toast( true );
            $message_show->position( 'top-end' );
            $message_show->showConfirmButton( false );
            $message_show->timer( 4500 );
            $message_show->timerProgressBar( 4500 );

            echo '<script>'. $message_show .'</script>';
    
        } elseif( isset($_SESSION['answer-toast']['success']) ) {
            $message_show = swal2_success( $_SESSION['answer-toast']['success']['title'] );
            $message_show->toast( true );
            $message_show->position( 'top-end' );
            $message_show->showConfirmButton( false );
            $message_show->timer( 4500 );
            $message_show->timerProgressBar( 4500 );

            echo '<script>'. $message_show .'</script>';
        }

    // Удалить сессию
    unset($_SESSION['answer-toast']);
}

// Сообщения об ошибке
if(isset($_SESSION['answer-error'])) {

    // ['answer-error']['title'] - для вывода заголовка
    $title = ( isset($_SESSION['answer-error']['title']) && !empty($_SESSION['answer-error']['title']) ) ? $_SESSION['answer-error']['title'] : '';

    // ['answer-error']['content'] - для вывода только текста
    // ['answer-error']['html'] - для вывода текста вместе с html тегами
    if( isset($_SESSION['answer-error']['content']) ){

        $message_show = swal2_error( $title, $_SESSION['answer-error']['content'] );

    } elseif (isset($_SESSION['answer-error']['html'])) {

        $message_show = swal2_error( $title );
        $message_show->html( $_SESSION['answer-error']['html'] );
    }

    // ['answer-error']['no-timer'] - отключение таймера закрытия окна
    // По умолчанию таймер закрытия окна установлен
    if( isset($_SESSION['answer-error']['no-timer']) ) {
        $message_show->timer(null);
        $message_show->timerProgressBar(null);
    } else {
        $message_show->timer(3500);
        $message_show->timerProgressBar(3500);
    }

    $message_show->showCloseButton(true);
    $message_show->showConfirmButton(false);
    echo '<script>'. $message_show .'</script>';

    // Удалить сессию
    unset($_SESSION['answer-error']);
}
?>