<!doctype html>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="/images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="/images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<title>Гранты и меры поддержки Тувы</title>
<meta name="description" content="">
<meta name="keywords" content="">

<!-- css -->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/css/hackathon.css">

<link rel="stylesheet" href="/css/responsive.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<div class="wrapper">
	<!-- <div class="preloader"></div> -->