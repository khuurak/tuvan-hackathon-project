
<script src="/js/jquery/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

<script src="/plugins/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="/plugins/jquery-validate/1.19.1/additional-methods.min.js"></script>
<script src="/js/jquery/jquery.inputmask.bundle.min.js"></script>

<script src="/plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="/plugins/typeahead/typeahead.min.js"></script>

<script src="/js/jquery/jquery.cookie.js"></script>

<!-- <script src="/js/script.js?ver=<?//=VERSION_JS?>"></script> -->

<?php
    // Подключение модального сообщения
    // Сообщения об успехе или об ошибке
    if( isset($_SESSION['answer']) || isset($_SESSION['answer-error']) || isset($_SESSION['answer-delete']) || isset($_SESSION['answer-toast']) ) {
        echo '<script src="/plugins/sweetalert2/sweetalert2.all.min.js"></script>';
        require_once LAYOUT_INC. '/alert.php';
    }
?>
<?=$this->getJavascript();?>
