<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Админ панель</title>
    <script>
      const global = globalThis;
    </script>
    <script type="module" crossorigin src="/plugins/react/index.66d1cd5f.js"></script>
    <link rel="modulepreload" href="/plugins/react/vendor.8a253bba.js">
    <link rel="stylesheet" href="/plugins/react/index.4f86c406.css">
</head>
<body>
    <div id="root"></div>

</body>
</html>