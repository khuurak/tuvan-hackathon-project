<?php 
// Подлючение шапка для шаблона
require_once LAYOUT_INC. '/html-head.php' ?>


<div class="container">     
    <?=$content?>
</div>


<?php 
// Подлючение подвала для шаблона
include_once LAYOUT_INC. '/footer.php' ?>

<?php 
// Подключение скриптов для шаблона
require_once LAYOUT_INC. '/scripts.php' ?>

</body>
</html>