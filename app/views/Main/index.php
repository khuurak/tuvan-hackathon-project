<?php 
// debug($news);
?>
	<div class="home5-slider">
		<div class="container-fluid">
			<div class="row">
				<div class="pogoSlider pogoSlider--dirCenterHorizontal pogoSlider--navBottom" id="js-main-slider">

					<div class="pogoSlider-slide bgc-overlay-black7" data-transition="slideRevealLeft" data-duration="1500" style="background-image: url(images/tuva-wh.jpg);">
						<div class="home_content">
							<div class="container text-center">
								<div class="slider-text1">Грант Главы Республики Тыва</div>
								<p>С 2023 года начнется прием заявок для участия в конкурсе на грант Главы Республики Тыва.</p>
							</div>
						</div>
					</div>
					<div class="pogoSlider-slide bgc-overlay-black7" data-transition="slideRevealLeft" data-duration="1500" style="background-image: url(images/tuva-wh.jpg);">
						<div class="home_content">
							<div class="container text-center">
								<div class="slider-text1">Грант Главы Республики Тыва</div>
								<p>С 2023 года начнется прием заявок для участия в конкурсе на грант Главы Республики Тыва.</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<section class="blog_post_container bgc-fa">
		<div class="container">
			<div class="row">
				<div class="col-l2">
					<div class="main_blog_post_content">

						<div class="row">

						<div class="col-12 text-center">
							<h2>Новости</h2>
						</div>
							
						<?php foreach($news as $v): ?>
							<div class="col-12 col-md-4">
								<div class="blog_grid_post mb35">
									<div class="details">
										<h4><?=$v['title']?></h4>
										<p><?=$v['description_short']?></p>

										<ul class="post_meta mt-2">
											<li><span class="flaticon-clock"></span> Опубликовано: <?=ucfirst_strtolower($v['date_create_format'])?></li>
										</ul>

										<a class="text-thm" href="/news/<?=$v['id']?>">Читать далее <span class="flaticon-right-arrow pl10"></span></a>
									</div>
								</div>
							</div>
						<?php endforeach;?>


					</div>

			</div>
		</div>
	</section>

	<section class="divider home5">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 offset-lg-2 text-center">
					<h1 class="color-white">Получите госпрограмму поддержки малого бизнеса в 2023</h1>
					<p class="color-white"></p>
					<a class="btn btn-thm divider-btn mt30" href="#">Подать заявку <span class="flaticon-right-arrow pl10"></span></a>
				</div>
			</div>
		</div>
	</section>

	<section class="testimonial-section bgc-fa">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="ulockd-main-title">
						<h3 class="mt0">Отзывы участников</h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="testimonial_slider">
						<div class="item">
							<div class="t_icon"><span class="flaticon-quotation-mark text-thm"></span></div>
							<div class="testimonial_post text-center p-2">
								<!-- <div class="thumb">
									<img class="img-fluid" src="images/testimonial/1.jpg" alt="1.jpg">
								</div> -->
								<div class="client_info">
									<h4>Фамилия Имя</h4>
									<p class="text-thm">Победитель гранта</p>
								</div>
								<div class="details">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus recusandae quam, veniam nam tenetur sequi facere hic voluptatum saepe magni aut nisi sed earum. Cupiditate animi necessitatibus reiciendis magnam blanditiis!</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="t_icon"><span class="flaticon-quotation-mark text-thm"></span></div>
							<div class="testimonial_post text-center p-2">
								<!-- <div class="thumb">
									<img class="img-fluid" src="images/testimonial/1.jpg" alt="1.jpg">
								</div> -->
								<div class="client_info">
									<h4>Фамилия Имя</h4>
									<p class="text-thm">Победитель гранта</p>
								</div>
								<div class="details">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus recusandae quam, veniam nam tenetur sequi facere hic voluptatum saepe magni aut nisi sed earum. Cupiditate animi necessitatibus reiciendis magnam blanditiis!</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>