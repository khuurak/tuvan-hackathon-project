<?php

namespace app\controllers;

use app\models\AppModel;
use app\models\DicsModel;
use app\models\DicsBodyModel;
use app\models\UsersModel;
use app\models\RolesModel;
use app\models\GrantsModel;

class ApiController extends AppController
{
  public function indexAction()
  {
    echo 'There is API page!';

    die;
  }

  public function accauntAction() {
    if( $_SERVER['REQUEST_METHOD'] === 'GET' ) {
      if (!isset($_SESSION['auth'])) {
        $obj = (object) [
          'error' => true,
          'text' => 'Вы не авторизованы'
        ];
      
        header('X-PHP-Response-Code: 401', true, 401);
        echo json_encode($obj);
      } else echo json_encode($_SESSION['auth']);
    }
    die;
  }

  public function signinAction()
  {
    $app_model = new AppModel;
    header('Content-Type: application/json; charset=utf-8');

    // проверка метода запроса (ПОСТ)
    if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

      // Присваиваем данные с формы переменным
      $email = ( isset($_POST['email']) && !empty($_POST['email']) ) ? $_POST['email'] : null;
      $password = ( isset($_POST['password']) && !empty($_POST['password']) ) ? $_POST['password'] : null;
  
      // Если введенные почта и пароль существуют и они не пустые
      if($email && $password) {

        // Если проверка была успешной, то авторизовываемся (создаем сессию AUTH) и перенаправляем в открытую страниц
        // Иначе, проверка была безуспешной
        if( $app_model->signin($email, $password) ) {
          $obj = (object) [
            'text' => 'Авторизация прошла успешно'
          ];
        
          echo json_encode($obj);
          die;
  
        } else {
          // Выводим сообщение о неверности пароля и перенаправляем в ту же странице
          $obj = (object) [
            'error' => true,
            'text' => 'Неправильный пароль'
          ];
        
          header('X-PHP-Response-Code: 401', true, 401);
          echo json_encode($obj);
          die;
        }
      }

      // Если номер телефон и пароль пустые
      header('X-PHP-Response-Code: 401', true, 401);

      die;
    }
    
    die;
  }

  public function signupAction()
  {
    $app_model = new AppModel;
    header('Content-Type: application/json; charset=utf-8');

    // проверка метода запроса (ПОСТ)
    if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

      // Присваиваем данные с формы переменным
      $phone = ( isset($_POST['phone']) && !empty($_POST['phone']) ) ? $_POST['phone'] : null;
      $email = ( isset($_POST['email']) && !empty($_POST['email']) ) ? $_POST['email'] : null;
      $password = ( isset($_POST['password']) && !empty($_POST['password']) ) ? $_POST['password'] : null;
  
      // Если введенные поля не пустые
      if($phone && $email && $password) {

        // Если проверка была успешной, то регистрируем и перенаправляем в открытую страниц
        // Иначе, проверка была безуспешной
        if( $app_model->signup($phone, $email, $password) ) {

          // Если регистрация была успешной, то авторизовываемся (создаем сессию AUTH) и перенаправляем в открытую страниц
          // Иначе, перенаправляем на страницу входа
          if( $app_model->signin($email, $password) ) {

            // Выводим сообщение о успешности авторизации и перенаправляем в открытую пользователем страницу
            $_SESSION['answer-toast']['success']['title'] = ' Вы успешно авторизовались!';

            $obj = (object) [
              'text' => 'Авторизация прошла успешно'
            ];
          
            echo json_encode($obj);
            die;
          }
  
        } else {
          // Выводим сообщение о неверности пароля и перенаправляем в ту же странице
          $obj = (object) [
            'error' => true,
            'text' => 'Ошибка регистрации'
          ];
        
          header('X-PHP-Response-Code: 401', true, 401);
          echo json_encode($obj);
          die;
        }
      }

      // Если номер телефон и пароль пустые
      return false;
    }

    // Подлючение стилей
    $this->setCss( [ '/css/signin.css?ver=' . VERSION_CSS ] );
  }

  // Метод для выхода
  public function logoutAction() 
  {
    if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
      header('Content-Type: application/json; charset=utf-8');

      // Если существует, то удаляем сессии и куки авторизованного пользователя 
      // и перенаправляем на главную страницу 
      if(isset($_SESSION['auth'])) unset($_SESSION['auth']);

      if(isset($_SESSION['verify'])) unset($_SESSION['verify']);

      $obj = (object) [
        'text' => 'Вы вышли из системы'
      ];
    
      echo json_encode($obj);

      die;
    }
  }

  // Справочники
  public function dicsAction()
  {
    $dics_model = new DicsModel;

    // выбор метода (CRUD)
    switch ( $_SERVER['REQUEST_METHOD'] ) {
      // получение всех справочников
      case 'GET':
        if (isset($_GET['code'])) 
          $result = $dics_model->getDics($_GET['code']);
        else
          $result = $dics_model->getDics();
        echo json_encode($result);
        die;
        break;
      // Добавление справочника
      case 'POST':        
        header('Content-Type: application/json; charset=utf-8');
        $_POST = json_decode(file_get_contents("php://input"), true);

        $name = ( isset($_POST['name']) && !empty($_POST['name']) ) ? $_POST['name'] : null;
				$code = ( isset($_POST['code']) && !empty($_POST['code']) ) ? $_POST['code'] : null;
				$descr = ( isset($_POST['descr']) && !empty($_POST['descr']) ) ? $_POST['descr'] : null;

        if (!is_null($name) || !is_null($code)) {
          // проверка на сущ-е по коду
          $one = $dics_model->getDics($code);
          if(count($one) > 0)
            $result = (object) [
              'error' => true,
              'text' => 'Тег с таким кодом существует'
            ];
          else $result = $dics_model->createDic($name, $code, $descr);
        } else {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля',
            'fields' => $name
          ];
        }

        echo json_encode($result);

        die;
        break;
      // Изменение справочника
      case 'PUT':
        header('Content-Type: application/json; charset=utf-8');
        $_POST = json_decode(file_get_contents("php://input"), true);

				$code = ( isset($_GET['code']) && !empty($_GET['code']) ) ? $_GET['code'] : null;
				$name = ( isset($_GET['name']) && !empty($_GET['name']) ) ? $_GET['name'] : null;
				$descr = ( isset($_GET['descr']) && !empty($_GET['descr']) ) ? $_GET['descr'] : null;

        if (is_null($code)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по коду
          $one = $dics_model->getDics($code);
          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Тег с таким кодом не найден'
            ];
            else {
              $name = is_null($name) ? $one['name'] || '' : $name;
              $descr = is_null($descr) ? $one['descr'] || '' : $descr;
              $result = $dics_model->updDic($code, $name, $descr);
            }
        }

        echo json_encode($result);
        die;
        break;
      // Удаление справочника
      case 'DELETE':
				$code = ( isset($_GET['code']) && !empty($_GET['code']) ) ? $_GET['code'] : null;

        if (is_null($code)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по коду
          $one = $dics_model->getDics($code);
          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Тег с таким кодом не найден'
            ];
          else $result = $dics_model->delDic($code);
        }

        echo json_encode($result);
        die;
        break;
    }
  }

  // Справочники | тело
  public function dicsbodyAction()
  {
    header('Content-Type: application/json; charset=utf-8');
    $_POST = json_decode(file_get_contents("php://input"), true);
    
    $dics_model = new DicsModel;
    $model = new DicsBodyModel;

    if (isset($_GET['dic_code']) || isset($_POST['dic_code'])) {
      
    } else {
      $result = (object) [
        'error' => true,
        'text' => 'Параметр "код справочника" обязателен'
      ];
      echo json_encode($result);
      die;
    }

    $dicCode = isset($_GET['dic_code']) ? $_GET['dic_code'] : $_POST['dic_code'];

    // выбор метода (CRUD)
    switch ( $_SERVER['REQUEST_METHOD'] ) {
      // получение всех значений
      case 'GET':
        if (isset($_GET['id'])) {
          $result = $model->getAllOrOne($dicCode, $_GET['id']);

          echo json_encode($result);
        } else {
          $result = $model->getAllOrOne($dicCode);

          echo json_encode($result);
        }
        die;
        break;
      // Добавление
      case 'POST':
        header('Content-Type: application/json; charset=utf-8');
        $_POST = json_decode(file_get_contents("php://input"), true);

        $name = ( isset($_POST['name']) && !empty($_POST['name']) ) ? $_POST['name'] : null;
        $descr = ( isset($_POST['descr']) && !empty($_POST['descr']) ) ? $_POST['descr'] : '';

        if (is_null($name)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else $result = $model->createOne($dicCode, $name, $descr);

        echo json_encode($result);

        die;
        break;
      // Изменение справочника
      case 'PUT':
        $name = ( isset($_GET['name']) && !empty($_GET['name']) ) ? $_GET['name'] : null;
        $descr = ( isset($_GET['descr']) && !empty($_GET['descr']) ) ? $_GET['descr'] : null;
        $id = ( isset($_GET['id']) && !empty($_GET['id']) ) ? $_GET['id'] : null;

        if (is_null($id)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по коду
          $one = $model->getAllOrOne($dicCode, $id);

          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Тег с таким кодом не найден'
            ];
          else {
            $name = is_null($name) ? $one['name'] || '' : $name;
            $descr = is_null($descr) ? $one['descr'] || '' : $descr;
            $result = $model->updOne($id, $name, $descr);
          }
        }

        echo json_encode($result);
        die;
        break;
      // Удаление справочника
      case 'DELETE':
        $id = ( isset($_GET['id']) && !empty($_GET['id']) ) ? $_GET['id'] : null;

        if (is_null($id)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по коду
          $one = $model->getAllOrOne($dicCode, $id);
          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Данные с таким кодом не найден'
            ];
          else $result = $model->delOne($id);
        }

        echo json_encode($result);
        die;
        break;
    }
  }

  // Роли
  public function rolesAction() {
    header('Content-Type: application/json; charset=utf-8');
    $model = new RolesModel;

    switch ( $_SERVER['REQUEST_METHOD'] ) {
      case 'GET':
        if (isset($_GET['id']))
          $result = $model->getAllOrOne($_GET['id']);
        else
          $result = $model->getAllOrOne();
        echo json_encode($result);
        die;
        break;
    }
  }

  // Пользователи
  public function usersAction() { 
    $model = new UsersModel;

    switch ( $_SERVER['REQUEST_METHOD'] ) {
      case 'GET':
        if (isset($_GET['uid']))
          $result = $model->getAllOrOne($_GET['uid']);
        else
          $result = $model->getAllOrOne();
        echo json_encode($result);
        die;
      case 'POST':
        $fam = ( isset($_POST['fam']) && !empty($_POST['fam']) ) ? $_POST['fam'] : null;
        $im = ( isset($_POST['im']) && !empty($_POST['im']) ) ? $_POST['im'] : null;
        $phone = ( isset($_POST['phone']) && !empty($_POST['phone']) ) ? $_POST['phone'] : null;
        $email = ( isset($_POST['email']) && !empty($_POST['email']) ) ? $_POST['email'] : null;
        $password = ( isset($_POST['password']) && !empty($_POST['password']) ) ? $_POST['password'] : null;

        if (is_null($email) || is_null($password)) {
          $result = (object) [
            'error' => true,
            'fields' => `$fam | $im | $phone | $email | $password`,
            'text' => 'd/ Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по email
          $one = $model->getOneIsEmail($email);
          if(count($one) > 0)
            $result = (object) [
              'error' => true,
              'text' => 'Пользователь с таким E-mail существует'
            ];
          else {
            // $result = $model->createOne($fam, $im, $phone, $email, $password);
          }
        }

        echo json_encode($result);
        die;
      break;
      case 'PUT':
        header('Content-Type: application/json; charset=utf-8');
        $_POST = json_decode(file_get_contents("php://input"), true);
        
        $uid = ( isset($_GET['uid']) && !empty($_GET['uid']) ) ? $_GET['uid'] : null;
        $fam = ( isset($_GET['fam']) && !empty($_GET['fam']) ) ? $_GET['fam'] : null;
        $im = ( isset($_GET['im']) && !empty($_GET['im']) ) ? $_GET['im'] : null;
        $phone = ( isset($_GET['phone']) && !empty($_GET['phone']) ) ? $_GET['phone'] : null;
        
        if (is_null($uid)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          $one = $model->getAllOrOne($uid);

          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Пользователь по UID не найден'
            ];
          else {
            $fam = is_null($fam) ? $one['fam'] || '' : $fam;
            $im = is_null($im) ? $one['im'] || '' : $im;
            $phone = is_null($phone) ? $one['phone'] || '' : $phone;
            $result = $model->updOne($uid, $fam, $im, $phone);
          }
        }

        echo json_encode($result);
        die;
      break;
      case 'DELETE':
        $uid = ( isset($_GET['uid']) && !empty($_GET['uid']) ) ? $_GET['uid'] : null;

        if (is_null($uid)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по коду
          $one = $model->getAllOrOne($uid);
          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Данные с таким кодом не найден'
            ];
          else $result = $model->delOne($uid);
        }

        echo json_encode($result);
        die;
      break;
    }
  }

  // гранты
  public function grantsAction() {
    header('Content-Type: application/json; charset=utf-8');
    $_POST = json_decode(file_get_contents('php://input'), true);
  
    $model = new GrantsModel;

    switch ( $_SERVER['REQUEST_METHOD'] ) {
      case 'GET':
        if (isset($_GET['code']))
          $result = $model->getAllOrOne($_GET['code']);
        else
          $result = $model->getAllOrOne(null, isset($_GET['my']) ? $_SESSION['auth']['identifier'] : null);

        echo json_encode($result);
        die;
      case 'POST':
        $date_finished = ( isset($_POST['date_finished']) && !empty($_POST['date_finished']) ) ? $_POST['date_finished'] : null;
        $date_start = ( isset($_POST['date_start']) && !empty($_POST['date_start']) ) ? $_POST['date_start'] : null;
        $descr_full = ( isset($_POST['descr_full']) && !empty($_POST['descr_full']) ) ? $_POST['descr_full'] : null;

        $descr_short = ( isset($_POST['descr_short']) && !empty($_POST['descr_short']) ) ? $_POST['descr_short'] : null;
        $finance_cofinancing = ( isset($_POST['finance_cofinancing']) && !empty($_POST['finance_cofinancing']) ) ? $_POST['finance_cofinancing'] : null;
        $finance_general_budget = ( isset($_POST['finance_general_budget']) && !empty($_POST['finance_general_budget']) ) ? $_POST['finance_general_budget'] : null;
        
        $finance_requested = ( isset($_POST['finance_requested']) && !empty($_POST['finance_requested']) ) ? $_POST['finance_requested'] : null;
        $info_geography = ( isset($_POST['info_geography']) && !empty($_POST['info_geography']) ) ? $_POST['info_geography'] : null;
        
        $info_goals = ( isset($_POST['info_goals']) && !empty($_POST['info_goals']) ) ? $_POST['info_goals'] : null;
        $info_social_significance = ( isset($_POST['info_social_significance']) && !empty($_POST['info_social_significance']) ) ? $_POST['info_social_significance'] : null;
        
        $info_target_groups = ( isset($_POST['info_target_groups']) && !empty($_POST['info_target_groups']) ) ? $_POST['info_target_groups'] : null;
        $info_tasks = ( isset($_POST['info_tasks']) && !empty($_POST['info_tasks']) ) ? $_POST['info_tasks'] : null;
        $name = ( isset($_POST['name']) && !empty($_POST['name']) ) ? $_POST['name'] : null;
        $grantDirection_id = ( isset($_POST['grantDirection_id']) && !empty($_POST['grantDirection_id']) ) ? $_POST['grantDirection_id'] : null;
        $contest_id = ( isset($_POST['contest_id']) && !empty($_POST['contest_id']) ) ? $_POST['contest_id'] : null;

        $result = $model->createOne($date_finished, $date_start, $descr_full, $descr_short, $finance_cofinancing,
          $finance_general_budget, $finance_requested, $info_geography, $info_goals, $info_social_significance,
          $info_target_groups, $info_tasks, $name, $grantDirection_id, $contest_id);

        echo json_encode($result);
        die;
      break;
      case 'PUT':
        $_POST = json_decode(file_get_contents('php://input'), true);
        
        $uid = ( isset($_GET['uid']) && !empty($_GET['uid']) ) ? $_GET['uid'] : null;
        $fam = ( isset($_GET['fam']) && !empty($_GET['fam']) ) ? $_GET['fam'] : null;
        $im = ( isset($_GET['im']) && !empty($_GET['im']) ) ? $_GET['im'] : null;
        $phone = ( isset($_GET['phone']) && !empty($_GET['phone']) ) ? $_GET['phone'] : null;

        
        if (is_null($uid)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          $one = $model->getAllOrOne($uid);

          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Пользователь по UID не найден'
            ];
          else {
            $fam = is_null($fam) ? $one['fam'] || '' : $fam;
            $im = is_null($im) ? $one['im'] || '' : $im;
            $phone = is_null($phone) ? $one['phone'] || '' : $phone;
            $result = $model->updOne($uid, $fam, $im, $phone);
          }
        }

        echo json_encode($result);
        die;
      break;
      case 'DELETE':
        $uid = ( isset($_GET['uid']) && !empty($_GET['uid']) ) ? $_GET['uid'] : null;

        if (is_null($uid)) {
          $result = (object) [
            'error' => true,
            'text' => 'Пропушенны обязательные поля'
          ];
        } else {
          // проверка на сущ-е по коду
          $one = $model->getAllOrOne($uid);
          if(count($one) < 1)
            $result = (object) [
              'error' => true,
              'text' => 'Данные с таким кодом не найден'
            ];
          else $result = $model->delOne($uid);
        }

        echo json_encode($result);
        die;
      break;
    }
  }

  // contest
  public function contestAction() {
    header('Content-Type: application/json; charset=utf-8');

    $query = "SELECT 
        t1.id,
        t1.title,
        t1.description,
        t1.date_start,
        DATE_FORMAT(t1.date_start, '%d %M %Y') AS 'date_start_format', 
        t1.date_finished,
        DATE_FORMAT(t1.date_finished, '%d %M %Y') AS 'date_finished_format',

        t2.id as go_id,
        t2.title as go_title

    FROM contests as t1
    LEFT JOIN gov_organizations as t2 ON t2.id = t1.gov_organization_id
    ";

    $contests = \R::getAll($query);

    echo json_encode($contests);
    die;
  }
}