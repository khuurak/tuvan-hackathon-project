<?php

namespace  app\controllers;

use app\models\GrantsModel;

class ProjectsController extends AppController
{
    public function indexAction() {
        
        $grants_model = new GrantsModel;

        $projects = $grants_model->getAllOrOne();

        // Отправляем метаданные в страницу
        $this->setMeta("Проекты");

        // Отправляем массив в страницу
		$this->set(compact('projects'));

    }

    public function viewAction() {
        
        // 
        // ПРОВЕРКА
        // 
        // Присвоение ID (GET параметра) с браузерной строки
        $id = $this->route['id'];

        // Если не существует id, 
        // то перенаправляем в страницу
        if( !isset($id) ) {
            redirect('/projects');
        }


        $grants_model = new GrantsModel;

        // Запрос в MySQL для получения месяца (даты) в русскоязычном виде 
        \R::exec("SET lc_time_names = 'ru_RU'");

        $project = $grants_model->getAllOrOne($id);

        // debug($project);

        // // Отправляем метаданные в страницу
        $this->setMeta($project['name']);

        // // Отправляем массив в страницу
		$this->set(compact('project'));

    }


}