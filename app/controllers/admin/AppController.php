<?php

namespace app\controllers\admin;

use siteCore\base\Controller;

class AppController extends Controller
{
    public $layout = 'admin';

    public function __construct($route)
    {
        // Доступ к странице
        // Если не существует сессии авторизации, то перенаправляем в главную страницу
        // if( !isset($_SESSION['auth']) ) {
        //     redirect('/signin');
        // }

        parent::__construct($route);
    }
}