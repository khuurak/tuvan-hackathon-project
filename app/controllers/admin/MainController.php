<?php

namespace app\controllers\admin;

use app\models\AppModel;

class MainController extends AppController
{
    public function indexAction()
    {
        // Отправляем метаданные в страницу
        $this->setMeta('Админ панель');
    }
}