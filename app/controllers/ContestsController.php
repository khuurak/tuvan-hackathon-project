<?php

namespace  app\controllers;

use app\models\GrantsModel;

class ContestsController extends AppController
{
    public function indexAction() {
        

        // Запрос в MySQL для получения месяца (даты) в русскоязычном виде 
        \R::exec("SET lc_time_names = 'ru_RU'");

        $query = "SELECT 
            t1.id,
            t1.title,
            t1.description,
            t1.date_start,
            DATE_FORMAT(t1.date_start, '%d %M %Y') AS 'date_start_format', 
            t1.date_finished,
            DATE_FORMAT(t1.date_finished, '%d %M %Y') AS 'date_finished_format',

            t2.id as go_id,
            t2.title as go_title

         FROM contests as t1
         LEFT JOIN gov_organizations as t2 ON t2.id = t1.gov_organization_id
         ";

        $contests = \R::getAll($query);

        // Отправляем метаданные в страницу
        $this->setMeta("Конкурсы");

        // Отправляем массив в страницу
		$this->set(compact('contests'));

    }

    public function viewAction() {
        
        // 
        // ПРОВЕРКА
        // 
        // Присвоение ID (GET параметра) с браузерной строки
        $id = $this->route['id'];

        // Если не существует id, 
        // то перенаправляем в страницу
        if( !isset($id) ) {
            redirect('/projects');
        }


        $grants_model = new GrantsModel;

        // Запрос в MySQL для получения месяца (даты) в русскоязычном виде 
        \R::exec("SET lc_time_names = 'ru_RU'");

        $project = $grants_model->getAllOrOne($id);

        // debug($project);

        // // Отправляем метаданные в страницу
        $this->setMeta($project['name']);

        // // Отправляем массив в страницу
		$this->set(compact('project'));

    }


}