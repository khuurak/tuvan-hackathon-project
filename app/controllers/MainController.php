<?php

namespace app\controllers;

use app\models\Main;

class MainController extends AppController
{
    public function indexAction()
    {
        // // Отправляем метаданные в страницу
        $this->setMeta('Гранты и меры поддержки Республики Тыва');

        // Запрос в MySQL для получения месяца (даты) в русскоязычном виде 
        \R::exec("SET lc_time_names = 'ru_RU'");

        $news = \R::getAll(
            "SELECT 
                id,
                title,
                description_short,
                DATE_FORMAT(date_create, '%d %M %Y %H:%s') AS 'date_create_format'
            FROM news ORDER BY date_create ASC"
        );

        // // Отправляем массив в страницу
		$this->set(compact('news'));
    }
}