<?php

namespace app\controllers\cabinet;

use app\models\AppModel;

class MainController extends AppController
{
    public function indexAction()
    {
        // Отправляем метаданные в страницу
        $this->setMeta('Мой кабинет');

        $app_model = new AppModel;

        $user_info = $app_model->getUser($_SESSION['auth']['identifier']);

        // Отправляем массив в страницу
        $this->set(compact('user_info'));
    }
}