<?php

namespace app\controllers\cabinet;

use siteCore\base\Controller;

class AppController extends Controller
{
    public $layout = 'cabinet';

    public function __construct($route)
    {
        // Доступ к странице
        // Если не существует сессии авторизации, то перенаправляем в главную страницу
        if( !isset($_SESSION['auth']) ) {
            redirect('/signin');
        }

        parent::__construct($route);
    }
}