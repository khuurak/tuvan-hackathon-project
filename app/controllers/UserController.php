<?php

namespace  app\controllers;

use app\models\AppModel;

class UserController extends AppController
{
	public function signinAction()
	{
		$this->setMeta('Вход на сайт', 'Страница входа в личный кабинет пользователя', 'вход, личный кабинет, профиль');

		$app_model = new AppModel;

		if( $_POST ) {

			// Присваиваем данные с формы переменным
			$email = ( isset($_POST['email']) && !empty($_POST['email']) ) ? $_POST['email'] : null;
			$password = ( isset($_POST['password']) && !empty($_POST['password']) ) ? $_POST['password'] : null;
	
			// Если введенные почта и пароль существуют и они не пустые
			if($email && $password) {

				// Если проверка была успешной, то авторизовываемся (создаем сессию AUTH) и перенаправляем в открытую страниц
				// Иначе, проверка была безуспешной
				if( $app_model->signin($email, $password) ) {

										// Выводим сообщение о успешности авторизации и перенаправляем в открытую пользователем страницу
					$_SESSION['answer-toast']['success']['title'] = ' Вы успешно авторизовались!';
					redirect('/cabinet');
	
				} else {
					// Выводим сообщение о неверности пароля и перенаправляем в ту же странице
					$_SESSION['answer-toast']['error']['title'] = 'К сожалению пароль не совпал';
					redirect();
				}
			}

			// Если номер телефон и пароль пустые
			return false;
		}

		// Подлючение стилей
		$this->setCss( [ '/css/signin.css?ver=' . VERSION_CSS ] );
	}

	public function signupAction()
	{
		$this->setMeta('Регистрация');

		$app_model = new AppModel;

		if( $_POST ) {

			// Присваиваем данные с формы переменным
			$phone = ( isset($_POST['phone']) && !empty($_POST['phone']) ) ? $_POST['phone'] : null;
			$email = ( isset($_POST['email']) && !empty($_POST['email']) ) ? $_POST['email'] : null;
			$password = ( isset($_POST['password']) && !empty($_POST['password']) ) ? $_POST['password'] : null;
	
			// Если введенные поля не пустые
			if($phone && $email && $password) {

				// Если проверка была успешной, то регистрируем и перенаправляем в открытую страниц
				// Иначе, проверка была безуспешной
				if( $app_model->signup($phone, $email, $password) ) {

					// Если регистрация была успешной, то авторизовываемся (создаем сессию AUTH) и перенаправляем в открытую страниц
					// Иначе, перенаправляем на страницу входа
					if( $app_model->signin($email, $password) ) {

						// Выводим сообщение о успешности авторизации и перенаправляем в открытую пользователем страницу
						$_SESSION['answer-toast']['success']['title'] = 'Вы успешно зарегистрировались';
						redirect('/cabinet');
		
					} else {
						// Выводим сообщение о неверности пароля и перенаправляем в ту же странице
						$_SESSION['answer-toast']['success']['title'] = 'Вы успешно зарегистрировались. Войдите пожалуйста';
						redirect();
					}
	
				} else {
					// Выводим сообщение о неверности пароля и перенаправляем в ту же странице
					$_SESSION['answer-error']['title'] = 'К сожалению не смогли зарегистрировать вас. Попробуйте еще раз';
					redirect();
				}
			}

			// Если номер телефон и пароль пустые
			return false;
		}

		// Подлючение стилей
		$this->setCss( [ '/css/signin.css?ver=' . VERSION_CSS ] );
	}

	// Метод для выхода
	public function logoutAction() 
	{
		// Если существует, то удаляем сессии и куки авторизованного пользователя 
		// и перенаправляем на главную страницу 
		if(isset($_SESSION['auth'])) unset($_SESSION['auth']);

		if(isset($_SESSION['verify'])) unset($_SESSION['verify']);

		redirect('/');

		die;
	}

}