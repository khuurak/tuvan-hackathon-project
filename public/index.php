<?php

// Подгружаем конфигурационный файл init c основыными константами define() и автолоадом psr-4
require_once dirname(__DIR__) . "/config/init.php";

// Маршрутизатор
require_once CONFIG . '/routes.php';

// Подгружаем пользовательские функции
require_once LIBS . '/functions.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Authorization, Origin');
header('Access-Control-Allow-Methods:  POST, PUT, GET, DELETE, OPTIONS');

new \siteCore\App();