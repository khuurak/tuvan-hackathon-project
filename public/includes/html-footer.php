<section class="footer_bottom_area home5 p0">
    <div class="container">
        <div class="row">
            <div class="col-12 pb10 pt10">
                <div class="copyright-widget tac-smd mt20">
                    <p>© 2022. Гранты и меры поддержки Республики Тыва - г. Кызыл, ул. Кочетова, д. 34а, тел.: +7983-516-5544, email: opora.tuva@mail.ru</p>
                </div>
            </div>
        </div>
    </div>
</section>

<a class="scrollToHome text-thm" href="#"><i class="flaticon-rocket-launch"></i></a>

<div class="sign_up_modal modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <ul class="sign_up_tab nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Вход</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Регистрация</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="login_form">
                        <form action="#">
                            <div class="heading">
                                <h3 class="text-center">Вход на сайт</h3>
                            </div>
                                <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                            </div>
                            <button type="submit" class="btn btn-log btn-block btn-thm">Войти</button>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="sign_up_form">
                        <div class="heading">
                            <h3 class="text-center">Создать аккаунт</h3>
                        </div>
                        <form action="#">
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputName1" placeholder="Телефон">
                            </div>
                                <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Пароль">
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck2">
                                <label class="form-check-label" for="exampleCheck2">Выбирая галочку и нажимая кнопку "Зарегистрироваться" вы соглашаетсь с <a href="/" class="text-thm" target="_blank">Политикой конфиденциальности</a> сайта</label>
                            </div>
                            <button type="submit" class="btn btn-log btn-block btn-dark">Зарегистрироваться</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div><!-- / .wrapper -->

<script type="text/javascript" src="js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/snackbar.min.js"></script>
<script type="text/javascript" src="js/simplebar.js"></script>
<script type="text/javascript" src="js/parallax.js"></script>
<script type="text/javascript" src="js/scrollto.js"></script>
<script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="js/jquery.counterup.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/progressbar.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script type="text/javascript" src="js/timepicker.js"></script>

<script type="text/javascript" src="js/script.js"></script>
</body>
</html>