<header class="header-nav menu_style_home_five navbar-scrolltofixed main-menu">
    <div class="container">
        <nav>
            <div class="menu-toggle">
                <img class="nav_logo_img img-fluid" src="images/logotype.png" alt="Гранты и меры поддержки Тувы">
                <button type="button" id="menu-btn">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <a href="/" class="navbar_brand float-left dn-smd">
                <img class="img-fluid mt10" src="images/logotype.png" alt="Гранты и меры поддержки Тувы" style="width: 270px;">
            </a>
            <ul id="respMenu" class="ace-responsive-menu" data-menu-style="horizontal">
                <li><a href="/main_files/hackathon/"><span class="title">Главная</span></a></li>
                <li><a href="/main_files/hackathon/contests.php"><span class="title">Конкурсы</span></a></li>
                <li><a href="/main_files/hackathon/projects.php"><span class="title">Проекты</span></a></li>
                <li><a href="#"><span class="title">Новости</span></a></li>
            </ul>
            <ul class="sign_up_btn pull-right dn-smd">
                <li><a href="#" class="btn btn-md btn-transparent" data-toggle="modal" data-target="#exampleModalCenter">Вход/Регистрация</a></li>
            </ul>
        </nav>
    </div>
</header>

<div id="page" class="stylehome1 h0 home5">
    <div class="mobile-menu">
        <div class="header stylehome1 home5 text-left">
            <img class="nav_logo_img img-fluid mt25" src="images/m-logotype.png" style="width: 200px;" alt="Гранты и меры поддержки Тувы">
            <a class="bg-thm" href="#menu"><span></span></a>
        </div>
    </div>
    <nav id="menu" class="stylehome1">
        <ul>
            <li><a class="text-thm" href="/main_files/hackathon/">Главная</a></li>
            <li><a class="text-thm" href="/main_files/hackathon/contests.php">Конкурсы</a></li>
            <li><a class="text-thm" href="/main_files/hackathon/projects.php">Проекты</a></li>
            <li><a class="text-thm" href="#">Новости</a></li>
            <li><a class="text-thm" href="#">Войти</a></li>
            <li><a class="text-thm" href="#">Зарегистрироваться</a></li>
        </ul>
    </nav>
</div>