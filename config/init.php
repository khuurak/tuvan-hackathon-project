<?

// КОНСТАНТЫ БАЗОВЫE

// Режим работы платформы. 
// Значения: 1 - режим разработки (development), все ошибки видны на загружаемой странице
// Значения: 0 - режим продакшна (production, рабочий режим для пользователей), все ошибки скрыты и записываются в лог файл
define("DEBUG", 1);

//Укаываем путь к корню сайта. 
define("ROOT", dirname(__DIR__));

// Указываем путь к публичной папке. 
define("WWW", ROOT. '/public');

// Указываем путь к папке структуры MVC. 
define("APP", ROOT. '/app');

// Указываем путь к папке ядра сайта. 
define("CORE", ROOT. '/vendor/site/core');

// Указываем путь к папке библиотек сайта. 
define("LIBS", ROOT. '/vendor/site/library');

// Указываем путь к папке кеша. 
define("CACHE", ROOT. '/tmp/cache');

// Указываем путь к папке конфигурации. 
define("CONFIG", ROOT. '/config');

// Название шаблона
define("LAYOUT", 'default');

// Указываем путь к папке includes общего шаблона. 
define("LAYOUT_INC", APP. '/views/layouts/includes');

// Указываем путь к папке includes для profile. 
define("PROFILE_INC", APP. '/views/profile/includes');

// Указываем путь к папке views. 
define("TEPMLATES_IN_VIEWS", APP. '/views');

// 
// SMS.RU
// 
// API ID на главной странице
define('SMSRU_API_ID', '6E1F8EBD-1440-8917-0A2C-8C54EDD8CFD5');

// 
// Telegram API Bot
// 
// Токен чат-бота @careerdo_notify_bot
define('TELEGRAM_TOKEN', '5507652585:AAHlM4vhrzUkY6dEc_HXK61c_31NPKXiqFQ');

// 
// Версии для JS и CSS файлов
// 
define('VERSION_JS', 1);
define('VERSION_CSS', 1);

// 
// Название сайта
// 
define("NAME_SITE", 'Tuvan hackathon project');

//
// Вычисляем url главной страницы
// 
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}"; // Вывод: https://site/public/index.php
$app_path = preg_replace("#[^/]+$#", '', $app_path); // С помощью preg_replace() вырезали index.php. Вывод: http://site/public/
$app_path = str_replace('/public/', '', $app_path); // С помощью str_replace() вырезали строку /Public/. Вывод: http://site
// Указываем путь главной страницы
define("PATH", $app_path);

// Указываем путь к папке админки
define("ADMIN", PATH. '/admin');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');

// ====
// Подгружаем автозагрузчик psr-4 композера. 
// Автозагрузчик помогает указывать короткий путь namespace при подключении классов
require_once ROOT. '/vendor/autoload.php';




