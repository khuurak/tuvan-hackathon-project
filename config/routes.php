<?php

use siteCore\Router;

// Маршруты входа, регистрации, выхода, страниц
Router::add('^signin$', ['controller' => 'User', 'action' => 'signin']);
Router::add('^signup$', ['controller' => 'User', 'action' => 'signup']);
Router::add('^verify$', ['controller' => 'User', 'action' => 'verify']);
Router::add('^logout$', ['controller' => 'User', 'action' => 'logout']);
Router::add('^getpassword$', ['controller' => 'User', 'action' => 'getpassword']);
Router::add('^public-offer$', ['controller' => 'Page', 'action' => 'publicOffer']);
Router::add('^privacy-policy$', ['controller' => 'Page', 'action' => 'privacyPolicy']);

// ADMIN
// Маршруты админки
Router::add('^admin$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
Router::add('^admin/*', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
// Router::add('^admin/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix' => 'admin']);

// PROFILE
// Маршруты личного кабинета пользователя 
Router::add('^cabinet$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'cabinet']);
Router::add('^cabinet/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix' => 'cabinet']);

Router::add('^api$', ['controller' => 'User', 'action' => 'index', 'prefix' => 'api']);

// FRONT
// Маршрут 
Router::add('^projects/(?P<id>[a-zA-Z0-9_-]+)/?$', ['controller' => 'Projects', 'action' => 'view']);
Router::add('^contests/(?P<id>[a-zA-Z0-9_-]+)/?$', ['controller' => 'Contests', 'action' => 'view']);

// Маршрут инфо-страницы пользователя
Router::add('^u/(?P<nickname>[a-zA-Z0-9_]+)/?$', ['controller' => 'User', 'action' => 'view']);
Router::add('^uid(?P<uid>[0-9]+)/?$', ['controller' => 'User', 'action' => 'view']);

// Маршруты по-умолчанию
Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');

